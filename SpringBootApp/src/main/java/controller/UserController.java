package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;

import model.User;
import repository.UserRepository;
import services.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService service;
	
	@PostMapping("/createUser")
	public void createUser(User user) {
		service.addUser(user);
	}
	
//	@PostMapping("userById/{id}")
//	public User findById(int id) {
////		return userService.
//	}
	
	@DeleteMapping
	public void deleteUser(int id) {
		
	}
}
