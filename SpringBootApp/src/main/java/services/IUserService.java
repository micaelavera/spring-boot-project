package services;

import java.util.ArrayList;
import java.util.List;

import model.User;

public interface IUserService {
	
	public void addUser(User user);
	
	public void updateUser(User user);
	
	public void deleteUser(User user);
	
	public List<User> listUsers();
}
