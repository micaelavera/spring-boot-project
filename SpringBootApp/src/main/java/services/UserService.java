package services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import model.User;
import repository.UserRepository;

public class UserService implements IUserService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public void addUser(User user) {
		repository.save(user);
	}

	@Override
	public void updateUser(User user) {
	}

	@Override
	public void deleteUser(User user) {
		repository.deleteById(user.getId());
	}

	@Override
	public List<User> listUsers() {
		return (List<User>) repository.findAll();
	}
}